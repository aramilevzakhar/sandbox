#include <iostream>

using namespace std;

int main(){
    long n;
    cin >> n;
    long sum=0;
    long min_sum=0;
    long ans;
    
    long p;
    for(int i=0;i<n;i++){
        cin>>p;
        if (i==0) ans=p;
        sum+=p;
        ans=(ans>sum-min_sum)? ans:sum-min_sum;
        min_sum=(min_sum<sum)? min_sum:sum;
    }
    cout<<ans<<endl;
    return 0;
}

#include <iostream>
#include <stdlib.h>
#include <ctime>

using namespace std;

int main() {
	srand(time(NULL));
	
	int n; 
	cout<<"n: ";
	cin >> n;
	long *s = new long[n];
	long *a=new long[n];
	
	for (int i = 0; i < n; i++) {
		a[i] = random() % 20 - 10;
		
		cout<<a[i]<<" ";
	}
	
	//int a[4]={1,2,3,4};
	//int s[4];
	//int n=4;
	cout<<endl<<endl;
	//a - это массив случайных чисел
	//s - это массив частичных сумм
	s[0]=0;
	int sum=0;
	for(int i=1;i<n+1;i++){
		s[i]=s[i-1]+a[i-1];
		cout<<s[i]<<endl;
	}
	int min_sum=0;
	int ans=0;
	for(int i=0;i<n+1;++i){
		ans=(ans > s[i]-min_sum)? ans : s[i]-min_sum;
		min_sum=(min_sum < s[i])? min_sum : s[i];
	}
	cout<<"answer: "<<ans<<" "<<min_sum<<endl;




	
	delete[] a;
	delete[] s;
	return 0;
}

